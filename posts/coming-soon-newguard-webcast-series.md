<!-- 
.. title: Coming Soon: Newguard Webcast Series
.. slug: coming-soon-newguard-webcast-series
.. date: 2017-09-10 20:56:09 UTC-04:00
.. tags: webcast
.. link: 
.. description: 
.. type: text
-->

**Beginning October 2017, New Guard will be hosting two webcasts each month for
technologists looking to up their game.**

During the second week of each month, we'll offer a **professional development
webcast**, covering business, communication, and other skills to help hackers get
ahead.  Then, during the fourth week of each month, we'll offer a **technical webcast**
covering a skill, tool, or other technical topic.  We're bringing in a variety
of speakers to keep things fresh and interesting.

Webcasts will be held live via Crowdcast (just join with your HTML5-compliant
browser of choice), where you can ask questions, chat and network with other
participants, and more. This blog will carry announcements of individual
webcasts' topics and speakers.  We'll also post recordings and any follow-up
materials after the webcasts are complete.

Tickets are just $5, but K-12 students and starving hackers are welcome to email
isaac@icei.org to inquire about the free tickets we have available.  We're also
supporting US veterans working their way into technical fields by offering free
tickets to Operation Code community members.

Register now for our inagural webcast:
[Productivity For Hackers](https://www.crowdcast.io/e/new-guard-prodev),
featuring ICEI Hacker-in-Chief Susan Sons.


