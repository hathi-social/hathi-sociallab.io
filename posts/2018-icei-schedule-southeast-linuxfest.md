<!-- 
.. title: ICEI Schedule for SouthEast LinuxFest 2018
.. slug: 2018-icei-schedule-southeast-linuxfest
.. date: 2018-06-01 17:00:00 UTC+02:00
.. tags: linuxfest, self
.. link: 
.. description: 
.. type: text
-->


The countdown for SELF has begun..!

Below is the ICEI track for the [South East Linux Fest (SELF)](http://www.southeastlinuxfest.org/) on Saturday, June 9th, 2018.  
Please feel free to follow us on [Twitter](https://twitter.com/ICEIorg).

<br />
# Schedule
| Timeslot&nbsp;  | &nbsp;&nbsp;&nbsp; | Event                                     | &nbsp;&nbsp;&nbsp; | Speaker                         | &nbsp;&nbsp;&nbsp; | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:---------------:| ------------------ |:------------------------------------------| ------------------ |:--------------------------------| ------------------ |:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0900-1000       |                    | Designing for security from the beginning |                    | Susan Sons                      |                    | Susan Sons will walk participants through the use of the Information Security Practice Principles to plan, evaluate, and refactor software architecture to meet security needs.<br />Susan is ICEI's President as well as the Chief Security Analyst at IU's Center for Applied Cybersecurity Research.<br />&nbsp;                                                                                                                                                                                                                    |
| 1015-1115       |                    | Newguard                                  |                    | Ian Bruene and Susan Sons       |                    | Susan Sons and Ian Bruene describe the saga of ICEI's New Guard, the mentoring program for new internet infrastructure software maintainers.<br />Come learn how you, too, can help support the future of a free, open, and secure internet.<br />&nbsp;                                                                                                                                                                                                                                                                               |
| 1130-1230       |                    | Newguard BoF                              |                    | ICEI Team                       |                    | Inspired by the Newguard talk? Just curious?<br />Hang out with some Newguardians and mentors, talk tech, and learn more about our community.<br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                              |
| 1230-1330       |                    | LUNCH                                     |                    |                                 |                    | <br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 1330-1430       |                    | War Stories lightning talk                |                    | TBD                             |                    | Come to share or listen to a series of five to six minute talks of technical disasters large and small.<br />All are welcome and any technological disaster story is welcome!<br />&nbsp;                                                                                                                                                                                                                                                                                                                                              |
| 1445-1545       |                    | Hathi                                     |                    | Ian Bruene                      |                    | Hathi is Yet Another decentralized communications project.<br />Covered are the Whys, Whats, and Why Should I Cares? of what we are doing.<br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                                 |
| 1600-1700       |                    | ICEI f2f                                  |                    | Susan Sons and the ICEI Team    |                    | Once per year, the Internet Civil Engineering Institute gathers some of our most active staff and community members together to talk about challenges and successes past, and spitball plans for the future.<br />All interested parties are welcome to join us.<br />&nbsp;                                                                                                                                                                                                                                                           |
| 1715-1815       |                    | NTPSec f2f                                |                    | Mark Atwood and the NTPSec Team |                    | At this meeting we will review our progress, discuss project strategy, and try to develop a shared sense of where we’re going in the following year.<br />As effective as online collaboration is, this sort of thing is still done best in meatspace.<br />The meeting will be open to past and present team members, all contributors, friends of the project, and interested parties from the horology community, system administration and network operations communities, and the open-source communities in general.<br />&nbsp; |
| 1815-1915       |                    | DINNER                                    |                    |                                 |                    | <br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 1915-2000       |                    | Prep for ICEI-cream social                |                    |                                 |                    | <br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 2000-EoD        |                    | ICEI-cream social                         |                    | No talking, just tasting.       |                    | Top secret special event..!<br />&nbsp;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

<br/>
# What is the SouthEast LinuxFest (SELF)?  
The SouthEast LinuxFest is a community event for anyone who wants to learn more about Linux and Open Source Software.  

It is part educational conference and part social gathering.  

Like Linux itself, it is shared with attendees of all skill levels to communicate tips and ideas, and to benefit all who use Linux and Open Source Software.

<br/>
# Practical information:
There are two options for registering for SouthEast LinuxFest.  
**Free registration** includes admittance and nothing else;  
**Paid registration** will include food.  
[Register](http://www.southeastlinuxfest.org/?page_id=30)

<br/>
# Venue:
Sheraton Charlotte Airport  
3315 Scott Futrell Dr  
Charlotte  
NC 28208
