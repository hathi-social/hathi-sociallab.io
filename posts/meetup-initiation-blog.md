<!-- 
.. title: New Guard Meetup
.. slug: newguard-meetup
.. date: 2018-03-22 18:00:00 UTC+01:00
.. tags: meetup
.. link: 
.. description: 
.. type: text
-->

New Guard will have a monthly get-together on ICEI's Discord server on each 4th sunday of the month.  
<br />
First Meetup meeting planned for  
**Sun 25 March, at 20 CEST / 14 EDT**  
<br />
**Come join us on Sunday through Discord**: [Invitation link](https://discord.gg/zswfvaK)  
Desktop client recommended; web client has issues with audio.  

<br />

# Concept
- Have a recurring meetup of staff, volunteers and passersby
- Provide an open space environment, and allow things to happen
- Support spontanous collaboration

<br />

# Purpose
- Sync / Idea bouncing
- Problem solving
- Information sharing
- Collaboration
- Internal project touch-ins
- Presentation practice
- Lightning talks

<br />

# Procedure
1. Define your purpose in going
1. Go to ICEI's server on Discord; invite link is https://discord.gg/zswfvaK
1. Go to “agenda-making” and help plan the agenda
   * Make topic suggestions
   * Let people know which topics you'd be interested in
   * Create a discussion
1. Join a discussion

## Rooms, and order of execution
The Meetup section has two rooms for discussions: "Moonbase Alpha" and "Odegra".  
One room consists of both a text channel and a voice channel.  
Discussions for a given room are held according to order of creation in #agenda-making.  

## Creating a discussion
Create a brainstorm/discussion/presentation by stating

* Topic:        Subject of the workshop
* Format:       E.g. discussion, brainstorm, presentation or information share
* Channel/chat: The virtual room in which the discussion will take place
* Chair:        The one leading a discussion, asking questions, presenting a subject, etc. Yourself, if you are the one creating the discussion.

## Chairing a discussion
1. Wait for the previous discussion to end.
1. Start your discussion by stating in #agenda-making that the discussion starts, and entering the room.
1. In the room, make the new discussion obvious by writing the topic in bold.
1. Lead the discussion as it takes place.
1. When you feel like it, end the discussion by stating so in #agenda-making, and leaving the room.

## Joining a discussion
1. Look through #agenda-making to see which discussions are going on, and which are about to happen.
1. Join the room in which your preferred discussion(s) are taking place whenever you feel like it.

<br />

# Expectations
- No "staying around to be polite"
- Follow your purpose in attending
- Join and leave discussions whenever you feel like it
- If you want something to happen, mention it in agenda-making and see if there's interest
- The Meetup starts at the indicated time, and ends whenever you feel like leaving
- Please test your audio setup before use
