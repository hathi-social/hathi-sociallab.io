<!--
.. title: Notes on Webcast: Productivity for Hackers
.. slug: notes-productivity-hackers
.. date: 2017-12-03 14:56:38 UTC-05:00
.. tags: notes, productivity
.. category: 
.. link: 
.. description: 
.. type: text
-->

# Table of Contents

1.  [Introduction](#org23f9321)
    1.  [Why look into productivity?](#orgc5f7d3f)
    2.  [Why not do so?](#org6a75377)
    3.  [It is not an innate talent](#orgc877559)
2.  [Rules](#org3c18154)
    1.  [Rule Zero: "DO NOT waste your valuable mental resources on remembering things."](#orgc5b4257)
    2.  [Rule One:  "Productivity is not a talent: It comes comes from disciplined iteration."](#org3671977)
3.  [How-To](#orgdad1026)
    1.  [Discipline](#org8fd150c)
    2.  [Structure: Tracking things and keeping them organized](#orgaedd372)
    3.  [Three sources of productivity](#orgc4b0a5d)
        1.  [Finding more time in which to work](#org71d59b3)
        2.  [Fitting more work into the time you have](#orgea020ff)
        3.  [Doing more impactful work](#org9c1d087)
    4.  [Optimizing](#orgf57aa79)
        1.  [Prototype the system](#org3309885)
        2.  [Test the system](#orgaf181c2)
        3.  [Evaluate the system](#org914c7c4)
        4.  [Analyze](#orgd4acbf6)
        5.  [Optimize](#orgbf1d074)
    5.  [Recharging](#org3adacaf)
    6.  [Tools](#orgfb453ca)
    7.  [Susan's own system](#org9b00c61)
        1.  [Workday mornings](#org0c7828f)
        2.  [During the Workday](#orgee28e7c)
        3.  [Meetings](#orgd14740f)
        4.  [Ending the workday](#org7c60abb)
4.  [Summary](#org866d55f)
5.  [Ressources](#org43bb67a)


<a id="org23f9321"></a>

# Introduction

Videos are excellent learning materials sometimes.  
But other times you are busy and not sure it's worth it listening to one hour of video. What's the content?  
And also, even having watched the video, there's some stuff you can't quite recall.  
So here's some notes ^.^  


<a id="orgc5f7d3f"></a>

## Why look into productivity?

-   Getting things done and out of the way
-   To have lower stress
-   To advance in life
-   To have more time to play
-   To be more effective
-   To get the most out of hobby and play activities


<a id="org6a75377"></a>

## Why not do so?

-   Typically "productivity" is something your boss talks about
    -   Or Dilbert's boss


<a id="orgc877559"></a>

## It is not an innate talent

-   Productivity is accomplished by learning a set of skills
-   It can be trained
-   It can be taught


<a id="org3c18154"></a>

# Rules


<a id="orgc5b4257"></a>

## Rule Zero: "DO NOT waste your valuable mental resources on remembering things."

-   We are smart and have nice brains
-   But try to remember things and it's actually creating a cyclic interrupt to 'make sure' which is stressful
-   Offload to a system and there is much more freedom to think
-   Get dates, details and to-dos out of your head and into a system as soon as possible
-   Be consistent: Trust the system so that you do not try to use your brain as a back-up, which would defeat the purpose


<a id="org3671977"></a>

## Rule One:  "Productivity is not a talent: It comes comes from disciplined iteration."

-   "10x developers", the myth
    -   They have innate talent
    -   They code faster because they are 'smarter'
-   "10x developers", the reality
    -   Does exist
    -   Productivity comes from discipline and iteration
    -   High-efficiency developers have good structure and good discipline
-   Current situation
    -   Noone talks about this discipline because there is a tendency to view these developers as rockstars
    -   More people have the potential to be a "10x dev", but most of them never make it there, because no-one taught them how


<a id="orgdad1026"></a>

# How-To


<a id="org8fd150c"></a>

## Discipline

-   What it isn't
    -   Doing what you're told
    -   Subject to the artist's conceit of "Waiting for inspiration"
    -   A focus on "What is the minimum I'm responsible for? What am I owed?"
-   What it is
    -   Doing the right thing regardless of convenience
    -   Constant iteration and improvement
    -   Being reliable to others
    -   A focus on "What can I do to help"
    -   Awareness of your ability to lead by example, regardless of your role
-   In summary
    -   Don't cop out.


<a id="orgaedd372"></a>

## Structure: Tracking things and keeping them organized

-   Action items
-   Notes from meetings / other comms
-   Ideas
-   Questions
-   Things to discuss with people later
-   Tips and How-tos
-   Anything you might want to remember
-   If it's not emergency, write down and take it up on regular scheduled meeting


<a id="orgc4b0a5d"></a>

## Three sources of productivity


<a id="org71d59b3"></a>

### Finding more time in which to work

-   Do time tracking, see where time is going
-   Utilize dead time: Carry work with you
    -   Meetings
    -   Transportation
    -   Waiting in line


<a id="orgea020ff"></a>

### Fitting more work into the time you have

-   Have tools be in good shape
-   Train self-discipline
    -   avoid rabbit holes
    -   avoid going off-mission
    -   really brilliant people get distracted; and by doing so they waste time and lose productivity
-   Track things and avoid wasting time
    -   Forgetting
    -   Confirming
    -   Being incorrect about details like deadlines and past decisions
    -   Getting blocked because you can't get information
-   Find time creep and quantify it
    -   Interruptions
        -   What are they costing you and when?
        -   It's 'just 5 minutes' but it echoes
            -   Knowledge workers hold complexities in their head to solve them; interruptions make them drop the pieces
            -   interruptions can cost 20-30 minutes to recap previous complexities
    -   Distractions
    -   Poor communication
    -   Multiple meetings on same subject
        -   Address this issue but quantify to prove it first
    -   Measuring interruptions
        -   Do similar task uninterrupted vs interrupted time
            -   Compare overall times
        -   Alternatively track interruption time separately
            -   Track reorientation - task timers can be helpful
-   Reliability
    -   The effect of lateness from one person multiplies with the number of people he or she is meeting with
    -   Doubt about whether or not someone shows up will automatically cause delays and cost other people their productivity
-   Have a purpose, and know what it is
    -   Don't do busywork
        -   It is a waste of time
        -   Hackers hate this anyway
    -   Goofing off is often avoidance behaviour due to being worried or not being certain what to do
    -   Knowing one's purpose and being prepared for it means being able to avoid the time-sinks other people cause for you
-   Scheduling is a big deal, especially for knowledge workers
    -   Someone else cannot easily take over, so our time and careful scheduling of it is important
    -   Careless scheduling will waste potential productivity very quickly


<a id="org9c1d087"></a>

### Doing more impactful work

-   Master the first two, or efforts on impact will fall short
-   Know yourself
    Deeply understand the team/org mission and your own interests: They should be aligned.
    Don't waste time on things that are unlikely to pay high dividends.
    If you've found the right team/org mission, working for the org will serve your own purposes as well.
-   Most bang for the buck
    -   "What lets me snowball?"
    -   Do the things that improve other things
    -   Grow your process, tooling and documentation to the point that the already-solved stuff can be delegated
    -   Actively mentor, or you will have no one to deletage to
    -   Look for or create processes, teams or groups that have similar or aligned goals
    -   If you're overloaded, create a system for what you are doing in order to delegate it
        -   If it's not clear enough to dump in the lap of someone of 1/5 your experience with minimal instruction, it's not a system
        -   As you master things, systematize as much as possible
    -   Build relationships with people more junior than you
        -   Junior people want to learn things that are interesting or valuable to them
        -   You will want to save time by delegating things you have already mastered but are overloaded with
        -   Building relationships in advance means that you will know when these interests are aligned
-   Optimize yourself
    -   It will delay what you're doing right now
    -   But it will make the next 10 things 20% easier or better
    -   Pick apart oft-repeated processes and look for ways to shave off time
    -   Be an example that builds up others; it creates impact and productivity for you, your team and organization
-   Optimize the team
    -   An aligned, focused and collaborating team will deliver more impact than any single person
    -   Build your skill at working with people very different from yourself: like-skilled people don't amplify one another
    -   Coordinate with others to avoid cross-work and duplication, look for chances to make different works build upon one another
    -   Find relationships between different things you are working on, and things you and your collegues are working on
        -   How can you avoid duplication of effort?
        -   Or make a sum greater than the parts?
    -   Active cross-mentorship between members of a team helps the team work together more smoothly, in additon to helping with individual problems a team member may need help with here or there
    -   Teams work best when they deeply understand one another's strengths and abilities, and feel free to leverage them


<a id="orgf57aa79"></a>

## Optimizing


<a id="org3309885"></a>

### Prototype the system

-   Use an organization system to eliminate forgetting, re-checking, and similar waste
-   Build a system to have a record which is always correct, and have no further time wasted once you've built a routine around it
-   Fits you and the way that you work
-   Makes your everyday work
-   Try to fit into the workflow that exists, because frankly you want their money


<a id="orgaf181c2"></a>

### Test the system

-   Prove your processes and tools
-   Keep track of things as quickly as possible
-   The longer you use your system, the more you will know what you want out of it


<a id="org914c7c4"></a>

### Evaluate the system

1.  Be aware

    -   Mindfulness
        -   Reclaim waste that seems beyond your control
        -   Train your mind to repeatedly re-evaluate what you are doing at the current moment
            -   Find the best takeaways from what you've done
            -   Decide what to keep doing and what to do next
        -   Don't just firefight; that's to ignore root causes and not learning

2.  Track things

    -   Document everything: remembering is an interruption
    -   Use a time tracker to find out where time is going
    -   If you experience pain points in your work flow, write the issue down so that you can deal with it later


<a id="orgd4acbf6"></a>

### Analyze

-   Regular review ensures that you are spending your time and energy on the right things
-   Are all the things necessary to do?
-   Are you the right person for the task at hand?
    -   Some things you are good at, other things other people are good at
        -   Play to strengths - using others for what they are good at saves the team, and you, time.
        -   Fit more work into the time you have
    -   It can make sense to delegate
        -   Would delegating let you perform tasks more important for the group?
        -   Can other people can learn what you're doing already?
        -   Can you teach it?
        -   Keep the flow going - enhance the capabilities of your group and rebalance the load if bottlenecks form
        -   Do more impactful work


<a id="orgbf1d074"></a>

### Optimize

-   Iterate over the three sources of productivity over time to optimize them
-   Get rid of the pain points
-   make the things you do faster and simpler
-   Eliminate things that force you to re-do things
-   Look for ways to improve tools and automation


<a id="org3adacaf"></a>

## Recharging

Downtime is good, but only if you're doing something restorative to you.
What recharges you?
(Net clicking isn't downtime - it isn't restorative)

-   Downtime
    -   Meetings with friends
    -   Be outside
    -   Have time for contemplation
    -   Have all physical needs met
    -   Is focus on relaxing and recharging when doing downtime? Or too much at work?


<a id="orgfb453ca"></a>

## Tools

-   There are benefits to going analog
    -   Save time otherwise spent on booting devices
    -   Notebooks do not have batteries that might die
-   Bullet Journal
    -   Freeform; keeps chaos organized
    -   Good if you have more notes than appointments
-   Action Day Planner
    -   Structure
    -   Good if more appointments than notes
-   Day-Timer / Franklin Covey
    -   Lots of structure
-   Emacs org-mode
    -   Specialized text editor mode for handling todo-lists, schedules, notes, and outlines for documents and books
    -   Hard learning curve for keyboard shortcuts but very fast to work in once accomplished
    -   Has smartphone app and synchronization
-   Productivity apps
    -   Always available
    -   Synchronizing


<a id="org9b00c61"></a>

## Susan's own system


<a id="org0c7828f"></a>

### Workday mornings

-   Before work: Normal tasks vs emergency tasks
-   At work: Drop bag, boot things, grab coffee, check in with people; costs only 20 minutes and will mean up to date with everything; do not schedule anything during first hour of work


<a id="orgee28e7c"></a>

### During the Workday

-   Follow lists, follow schedule, everything is structured, knows everything that happens, no interruptions because already checked in
-   Remember things by marking up in the journal
-   No need to have answers ready in head; use journal. Looks organized, too
-   Test if you really need to check email often; it costs productivity
-   Prioritization: If something else is important and there is no time
    -   Can other stuff be dropped
    -   Can it be delegated
    -   Can it wait


<a id="orgd14740f"></a>

### Meetings

-   Alarms
    -   Stateful work vs alarm interruptions
        -   Better than the constant interrupts of from the brain
        -   Blending-in alarms of smartphones, starts slowly and warms up. Helps
-   Time-wasting meetings
    -   Do a little work
    -   Plan ahead
    -   Notes can be taken, books sifted


<a id="org7c60abb"></a>

### Ending the workday

-   Triage: Nice-to-haves might need to get dropped in order to keep focus
-   Avoid constantly growing todo-lists
-   When the meat of the workday is done, move to end-of-work and stop working.
-   Work-times can vary, but if it's done, don't linger; pack up stuff and get out the door
-   When it's over, it's over. Stop working if brain is done. Recognizing these symptoms and reacting properly makes you more productive.
-   Leave two evenings and a weekend day as cushion for emergencies
-   Don't plan every minute of your life; it's a trap
-   Glance at next day's things


<a id="org866d55f"></a>

# Summary

-   Get a system, your brain is too valuable for storage and related interrupts
-   Productivity comes from:
    -   Finding more time in which to do work
    -   Fitting more work into the time you have
    -   Doing more impactful work
-   Discipline will get you there - always trying to make things a little better, be sceptical about waste
-   Complex, ambitious projects succeed due to collective productivity


<a id="org43bb67a"></a>

# Resources

-   [A Hacker's Guide to Getting Ahead (draft)](https://binaryredneck.net/hacker-getting-ahead/)
-   Susan herself
    -   More New Guard webcasts if there's interest
    -   O3s for ICEI staff
-   [Dave Seah's productivity blog](http://davidseah.com/blog/)
-   The internet; collect all it has to offer, then discard most of it
-   Your own experiments. The world requires experimentations
-   [Slides for the webcast](http://slides.com/hedgemage/productivity-for-hackers#/)
-   [Bullet Journal](https://bulletjournal.com/)
-   [Emacs Org-mode](http://orgmode.org/)

