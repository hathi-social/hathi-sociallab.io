<!--
.. title: Starter Guide
.. slug: starter-guide
.. date: 2017-10-08 18:02:00 UTC-04:00
.. tags: knowledge, information, library
.. category: 
.. link: 
.. description: 
.. type: text
-->

The following page is intended as a 'how to' to ease the path of getting started in New Guard, as the first step is often the hardest one to take.  

1. [Introduction](#introduction)
1. [Expectations and work environment](#expectations)
1. [Newguard Skill Set](#skillset)
1. [Books](#books)
1. [Contact Information](#contact-information)
<br />

-----------------------------------------------------------------------

<a id="introduction"></a>
<br />

# Introduction

New Guard is a mentoring group, and as such we are doing a little extra in order to ease the path and lessen the obstacles to getting started on OSS and specifically infrastructure development.  
<br />
You are welcome to hold us to this statement. In fact, if you find yourself in doubt about how or where to start on something, or how this OSS is supposed to work, or who to contact, we would like to know about it (and help out).  
<br />
The reason that we want to do this is that we find that there's not that many developers left with experience in infrastructure development; more about this issue [here](https://boingboing.net/2016/11/11/the-internets-core-infrastru.html) and [here](http://observer.com/2016/11/open-source-too-big-to-fail/).  
It is to meet this challenge that ICEI has formed New Guard.  
<br />
New Guard has two types of goals: Group goals and project goals.  
The goal of New Guard as a group is to provide mentoring, work experience and vetting to volunteers interested in OSS development, primarily focusing on infrastructure and security systems.  
The goal of New Guard's current project is to ensure a stable, efficient and freely available collaboration environment for OSS development.  
<br />

-----------------------------------------------------------------------

<a id="expectations"></a>
<br />
# Expectations and work environment

## We'll give guidance
If you are interested in working on a project, we will invest time in guiding you and help you find a role and tasks that fit your preferences.  
If you spot a task that you're interested in, want to get to work immediately and already know what to do, you are of course very welcome to help us out; but if it all seems a bit chaotic and it's not clear what you can do or what is needed, please contact us and we'll work it out.  

## One-on-ones
For volunteers dedicating themselves to a project for a period, we offer one-on-ones with the project manager in order to provide a sounding board, give you feedback on accomplishments and coordinate your efforts with the rest of the team.  
If you're interested in feedback on something in particular, such as code legibility, specification writing or other areas, let us know and we'll try to find a way to provide it.  

## Expectations
We expect you to have a personal goal.
It might be curiousity, or to get better in a certain skill, or that you have your own use for the finished project - whatever the case, the stronger your interest in your goal and the more that goal is aligned with the goals of Icei and the project, the more certain we can be as a group that you will continue to be a consistent contributor.  
For team members, we also expect you to give us notice if you are no longer able to contribute.  
It's a fact of life that things can get in the way, and that priorities can shift. But with a timely notice we will know what is going on, be able to re-delegate and avoid needless delays.  
If you can manage: help others, in the team or in the group. It'll create ties, enhance communication and, since our goals are aligned - help you accomplish your own goals.

<br />

<!--
Relational; mutual benefits

- New Guard roles at o3s
    - Feedback on goals, accountability
    - Cohesion
    - Skills mentoring
    - Career mentoring
- Guidance
    - If you are interested in working on a project, we will invest time in you and help you find a role and tasks that fit your preferences
    - Expertise provided for project work
    - o3s with mentor feedback on a 12-1 ratio? Can we do that? Do we have willing mentors?
-   Management
    - Perhaps not management as you know it
    - Cohesion
    - Remove obstacles
    - Ensure communication and clarity
    - Obtain resources
    - Ensure accountability
    - How
      - Weekly o3 followup meetings
      - Manager-Tools
      - Flexibility
- Mentoring
    - Expertise provided for project work
    - Skill training feedback
    - o3s with mentor feedback on a 12-1 ratio? Can we do that? Do we have willing mentors?
- Expectations
    - Aligned interests
        - Ideals
        - Work experience
        - Learning new skills
    - Consistency and being up-front
    - Mutually beneficial relation

<br />


Another thing you can expect is influence on project choices.  
The caveat is that this influence will largely correspond to the work you put into the project.  
<br />
If you are not sure where to start, the managers of either project will be able to guide you to a spot.  
A couple of openings are posted for each project; these are the roles that we can easily define and where opportunities for helping out are many. This means that we can give you clear goals and starting points.  
If you'd rather work on something else, though, let us know; just because we didn't think of it doesn't mean it's not needed.  
<br />
Learning is self-guided, but we will provide some introduction and guidance on where to fit in.  

Irc is often the fastest way of getting answers to specific questions, and also the best place to socialize - but in addition to that, the contact person for New Guard (Uffe) is also available by mail and Discord.  
Contact information is at the bottom of this page.  
<br />
Our "Featured" icei projects are still in their early phases and help for the teams will be appreciated.  
During collaboration and development, some concepts and methodologies will often be referred to, and we'll collect the most common here, in particular those related to infrastructure developement.  
We also do matchmaking for Newguardians with old-school infrastructure software maintainers to give the New Guard an opportunity to work under, and learn from, those who came before them.  
<br />
-->


-----------------------------------------------------------------------

<a id="skillset"></a>
<br />

# Newguard Skill Set

As ICEI's mentoring group, Newguard focuses its efforts on  

1. Skills that are useful to ICEI
2. Skills that we have the knowledge to teach

Happily we have 100% overlap between these skills.  

The skill sets that we focus on imparting to volunteers is  
<br />

* **Infrastructure**
* **Security**
* **FOSS Development**
* **Management**
* **How to work together on a team project**

<br />
<A HREF="/images/newguard-skill-set.png"><IMG WIDTH=800 SRC="/images/newguard-skill-set.png"></A>  

These are the skills that ICEI is drawing on on a daily basis; this means both that it is important for volunteers to acquire proficiency in one or more of the branches, and also that these are skill sets that Newguard will be actively covering.  

Many of the topics are already covered to a degree, by the books listed below and by the webcasts we are producing and broadcasting, but the idea is to provide a full curriculum, a training ground, and mentoring.

<br />

-----------------------------------------------------------------------

<a id="books"></a>
<br />

# Books

Books are available for most fields in software development, but for large-scale system infrastructure those books are few and far between.  
These are the ones we would recommend in particular.

<br />
<br />

![The Art of UNIX Programming][artunixprog]  
## The Art of UNIX Programming
*The Art of Unix Programming attempts to capture the engineering wisdom and philosophy of the Unix community as it's applied today — not merely as it has been written down in the past, but as a living "special transmission, outside the scriptures" passed from guru to guru. Accordingly, the book doesn't focus so much on "what" as on "why", showing the connection between Unix philosophy and practice through case studies in widely available open-source software.*  
*-Eric S. Raymond*  

<br />
<br />

![The Cathedral & the Bazaar][cathedralbazaar]  
## The Cathedral & the Baazaar
*"The Cathedral and the Bazaar: Musings on Linux and Open Source" by an Accidental Revolutionary (abbreviated CatB) is an essay, and later a book, by Eric S. Raymond on software engineering methods, based on his observations of the Linux kernel development process and his experiences managing an open source project, fetchmail. It examines the struggle between top-down and bottom-up design. The essay was first presented by the author at the Linux Kongress on May 27, 1997 in Würzburg and was published as part of the book in 1999.*  
*-Wikipedia*

<br />
<br />

![Cryptography Engineering][cryptographyengineering]  
## Cryptography Engineering: Design Principles and Practical Applications
*Dive deeply into specific, concrete cryptographic protocols and learn why certain decisions were made. Recognize the challenges and how to overcome them.*  
*Understand what goes into designing cryptographic protocols*  
*Develop an understanding of the interface between cryptography and the surrounding system, including people, economics, hardware, software, ethics, policy, and other aspects of the real world*  
*Look beyond the security protocol to see weaknesses in the surrounding system*  
*Thwart the adversary by understanding how adversaries think*  
*-Schneier.com*

<br />
<br />

![TCP/IP Illustrated vol. I][tcpipillustratedI]  
## TCP/IP Illustrated, Volume 1 - The Protocols
*TCP/IP Illustrated is the name of a series of 3 books written by W. Richard Stevens. Unlike traditional books which explain the RFC specifications, Stevens goes into great detail using actual network traces to describe the protocol, hence its 'Illustrated' title.*  
*-Wikipedia*

<br />
<br />

![the Mythical Man-Month][mythicalmanmonth]  
## The Mythical Man-Month
*Brooks discusses several causes of scheduling failures. The most enduring is his discussion of Brooks's law: Adding manpower to a late software project makes it later. Man-month is a hypothetical unit of work representing the work done by one person in one month; Brooks' law says that the possibility of measuring useful work in man-months is a myth, and is hence the centerpiece of the book.*  
*Complex programming projects cannot be perfectly partitioned into discrete tasks that can be worked on without communication between the workers and without establishing a set of complex interrelationships between tasks and the workers performing them.*  
*Therefore, assigning more programmers to a project running behind schedule will make it even later. This is because the time required for the new programmers to learn about the project and the increased communication overhead will consume an ever increasing quantity of the calendar time available. When n people have to communicate among themselves, as n increases, their output decreases and when it becomes negative the project is delayed further with every person added.*  
*-Wikipedia*
<br />

-----------------------------------------------------------------------

<a id="contact-information"></a>
<br />

## Contact Information

Our IRC channel is #newguard on irc.freenode.net.  
This is where most group interaction happens and where basic questions will be answered the quickest.  
<br />
The contact person for New Guard is Uffe Wassmann (me); I'm available through mail to uw (at) icei.org, and also through Discord to nick Fluxus#0028.  


<!--
## Useful knowledge
* OSS development practices?
* Standards?
* Tools?

## Websites and blogs
* From the Trenches
  - Stories
  - Opiniated pieces
  - Infrastructure
  - Tech
  - Development
  - Anything else that seems relevant
-------------
-->

[artunixprog]: /images/art-unix-programming_cover.png "The Art of Unix Programming"
[cathedralbazaar]: /images/cathedral-bazaar_cover.jpg "The Cathedral & the Bazaar"
[tcpipillustratedI]: /images/tcp-ip-illustrated_cover.jpg "TCP/IP Illustrated, volume I"
[mythicalmanmonth]: /images/mythical-man-month_cover.jpg "The Mythical Man Month"
[cryptographyengineering]: /images/cryptography-engineering_cover.jpg "Cryptography Engineering"
