<!-- 
.. title: 2018 SouthEast LinuxFest
.. slug: 2018-southeast-linuxfest
.. date: 2018-04-13 22:00:00 UTC+01:00
.. tags: linuxfest, self
.. link: 
.. description: 
.. type: text
-->

ICEI will be participating in [SouthEast LinuxFest](http://www.southeastlinuxfest.org/), and our annual face-to-face meeting will also take place at this time.

<br/>
# What is the SouthEast LinuxFest (SELF)?  
The SouthEast LinuxFest is a community event for anyone who wants to learn more about Linux and Open Source Software.  

It is part educational conference and part social gathering.  

Like Linux itself, it is shared with attendees of all skill levels to communicate tips and ideas, and to benefit all who use Linux and Open Source Software.

<br/>
# What will ICEI be doing?
## Presentations
- Newguard: ICEI Mentoring Group Initiative
- Hathi: Decentralized Collaboration Network
- Designing For Security

## Panels
- Rescuing Bitrotted Software


## Meetings
- ICEI face to face meeting
- NTPsec face to face meeting

..and also there are rumours of ice-cream related shenanigans.

<br/>
# Practical information:
[Book accomodation](http://www.southeastlinuxfest.org/?page_id=23) soon as there's few hotel rooms left at the special SELF discount rate - although yes, there are other hotels available.

There are two options for registering for SouthEast LinuxFest.  
**Free registration** includes admittance and nothing else;  
**Paid registration** will include food.  
[Register](http://www.southeastlinuxfest.org/?page_id=30)

<br/>
# Venue:
Sheraton Charlotte Airport  
3315 Scott Futrell Dr  
Charlotte  
NC 28208
