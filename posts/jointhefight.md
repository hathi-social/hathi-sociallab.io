<!--
.. title: Join the Fight!
.. slug: jointhefight
.. date: 2018-06-04 18:32:00 UTC+02:00
.. tags: newguard, howto, calltoarms
.. category: 
.. link: 
.. description: 
.. type: text
-->


## Adventure overview

* **1: Select your class**  
  Choose what you wish to become.  
* **2: Select your roles**  
  Choose the skills which you wish to train.  
  Usually closely related to your class and intended to gain experience with that class.
* **3: Select a project**  
  Your battleground of choice.  
  We have a "brief" overview of terrain types here.  
* **4: Select your quest**  
  Bits of advice on taking quests; depending on goals and circumstances, you may wish to take on either beginner quests or advanced quests.  
* **5: Evaluate**  
  On getting feedback.  
* **6: New Horizons**  
  Where to go next.


<br />
# 1: Select your class

- **Algorithmicist**  
  Very good at algorithms and sustained, intricate coding.  
  Have mathematical intuition, and are one of the two types (with Architect) that have the highest tolerance for complexity.  
  They like the idea of correctness proofs and think naturally in terms of invariants.  
  They gravitate to compiler-writing and crypto.  
  Often solitary with poor social skills; have a tendency to fail by excessive cleverness.  
  Never let them manage anyone!

- **Tinkerer**  
  Hackers who are drawn to crossovers with the physical world – will design hardware as cheerfully as software.  
  One of the two types (with Prankster) most likely to be lockpickers and locksmiths.  
  Know practical electronics (including analog and RF), adept at reverse-engineering.  
  When you can get them to pull their heads out of the details (which they may resist pretty hard) they make terrific whole-systems engineers.

- **Architect**  
  The guys who are fascinated by, and very good at, blocking out architecture in complex systems.  
  Kings of the productive refactor.  
  Have an acute feel for design patterns and can see around corners in design space.  
  Strong drive to simplify and partition; “It’s not done until it’s elegant.”  
  The Architect failure mode is to lose sight of the ground.  
  Architects don’t necessarily have communications skills; if they do, they can make worthy team leads.

- **Sharpshooter**  
  Tenacious detail-obsessives who are most comfortable with a bottom-up view of code and like rifle-shooting bugs more than almost anything else.  
  In past times they would have been happy writing assembler.  
  Polar opposite of the Architect, very productive when paired with one (and vice-versa).  
  Not a good bet for managing anything.

- **Jack-of-All-Trades**  
  The great strengths of the jack-of-all-trades are adaptability, fast uptake of new ideas, and mental flexibility.  
  The JOAT doesn’t do any one thing better than the other types, but can do a bit of everything – including people and social engineering.  
  The JOAT failure mode is to try to do everything themselves.  
  A JOAT is more likely than other types to make an excellent team lead, as long as he or she remains aware enough to delegate deep technical decisions to others.

- **Prankster**  
  Their natural bent is adversarial – they’re great at thinking up ways to disrupt and subvert systems (or just put them to unexpected and hilarious uses).  
  They gravitate to infosec and test engineering.  
  The really good ones can social-engineer people more ruthlessly and effectively than any of the other types.

- **Castellan**  
  Supreme control freaks who get their power from focusing on what they’re responsible for and knowing it inside out.  
  Castellans memorize manuals; they love language-lawyering, process automation, and vacuuming up domain-specific knowledge about whatever they’re working on.  
  Old-school sysadmins are often castellans: “That will never happen on my system” is their promise (and of course Pranksters love to prove them wrong).

- **Translator**  
  The type that bridges between human and machine: tends to excel at UI/UX development, documentation, policy and supply-chain stuff, requirements analysis, user training, and so on.  
  Highly social, less hard-core technical than others, but in a way that helps them help other hackers understand how non-hackers see and interact with technology.  
  Some of them make good project managers, but like JOATs they need to understand their technical limitations and mostly leave the hard decisions to types that naturally swim in deeper technical waters.  
  Of all the types, Translators are the least likely to self-identify as hackers even if they are intimate with the culture and working within it.

*Source for classes is [Hacker Archetypes](http://esr.ibiblio.org/?p=7478), as described on Armed & Dangerous by Eric S. Raymond.*

<br />
# 2: Select your roles

- **Documenter**  
  Write installation guides; explain black magic.  

- **Tester**  
  See if things work as expected when you are doing things the right way.  
  Then try out the wrong way.  

- **Designer**  
  Have a concept explained or make up your own;  
  Sketch out how to go about it;  
  Create skeleton code to show the way.  

- **Implementer**  
  Make things work and do it right.  

- **Code reviewer**  
  Make sure that everything is, in fact, done right.  

- **Architect**  
  Decide how things are put together.  

- **Archivist**  
  Track architectural choices made;  
  Write these down so as not to confuse later archeologists too much.  

- **Task coordinator**  
  Optimize delegation of tasks so that  
  1: Everyone is happy with their tasks  
  2: No one needs to wait for someone else to finish  
  3: The most important thing is done first  

- **Accountant**  
  Make sure that progress is being made, and communicate status and progress to the rest of the team.

- **Sounding board**  
  Be one with the rubber duck.  
  Listen carefully.  
  Ask "Why?"  
  Mention unmentioned alternatives.  
  Agree with people whenever they solve their problems.  

- **Planner**  
  Figure out what is necessary for the team to do in time for them to do it.  
  Figure out what each team member likes to work on and make it available to them if possible.  
  Decide in which order to have the tasks done.  
  Predict what is going to be ready when.  
  Provide the other team members an overview so that they always know what is happening when.  

- **Nagger**  
  Remind people of meetings, promises, and everything else they don't like being reminded of  

- **Troubleshooter**  
  If a task stops progressing, it's because there is trouble.  
  Troubleshooters do whatever is necessary to shoot that trouble down.  

- **Mentor**  
  Listen to what people want.  
  Point them in the right direction to make it happen.  
  Give advice if they encounter obstacles along the way.  
  Give them tasks they can learn from.  
  Watch them grow.  

<br />
# 3: Select a project
In contrast to commercial projects that pays wages, when doing volunteer work you have a lot of choices available.  
Many, many choices.  
<br />
We've summarized the major types of open source projects below, so as to present a bit of an overview.  
The thing is, many things are open source, but not all open source projects will fit your needs at all times.  
Depending on your skill level, the time you have available, and whether you are doing this for idealistic reasons, learning opportunities or financial reasons, you will want to look into different types of projects.  
<br />

Newguard has its own projects on offer here:

<br />

* **[Hathi](https://newguard.icei.org/hathi-project/)** is a decentralized collaboration framework aimed at productivity, privacy and robustness.  
It's a project which is still in its beginning phases but the main architecture has been hashed out; it is a project with a lot of possibilities open.  
Hathi is particularly looking for designers, implementers, documenters and architects.  
In terms of the project archetypes below, Hathi is part Rocket Ship to Mars while we're en route to version 1.0, with an aim to go towards Controlled Ecosystem and later Wide Open.  
<br />
* **[NTPsec](https://www.ntpsec.org/)** is an ICEI rescue project implementing the Network Time Protocol - which is what keeps accurate time across the internet.  
The project has a well-established core team but could easily use troubleshooters, testers, and archivists.  
In terms of project archetypes, it is a Specialty Library.  

<br />
The main attraction of Newguard projects as opposed to numerous other open source projects is our interest in creating and maintaining a peer-to-peer learning environment.  
We make an effort to explain what open source development is about, and how to navigate career paths.  
First and foremost, though, ICEI is about infrastructure & security; it is if you are attracted to these fields that you will get the most out of collaborating in Newguard and/or ICEI projects.  
<br />
## Open Source Project Archetypes

### Business-to-Business (B2B) Open Source

This archetype aims at ubiquity.  
Its open source nature is designed to spur OEM adoption by partners and competitors across the industry and to drive out competing options in the same way pricing browsers at zero once discouraged new entrants into the browser market.  
It can also drive down the price of complementary products: for example, as Red Hat Enterprise Linux (RHEL) does for server hardware and Google's Android operating system does for handheld devices.  

* **Examples**  
Android, Chromium  

* **Characteristics**  
Solid control by founder and (sometimes grudging) acceptance of unpredictability by redistributors.
This also implies a long fork-modify-merge cycle for partners, and thus a constant temptation on their part to fork permanently.

* **Community standards**  
In general, the lead company does not put much emphasis on welcoming or nurturing contributors; exceptions may be made for strategically important organizational partners.  

* **Typical governance**  
Maintainer-led by a group within the lead company.  


<br />
### Multi-Vendor Infrastructure

Multi-Vendor Infrastructure efforts are driven by business needs rather than
individuals, and are open to relatively lightweight participation.  
A good way to distinguish a Multi-Vendor Infrastructure project from
Business-to-Business (B2B) Open Source is to look at how often contributors
continue to participate after they switch employers.  
When that phenomenon occurs, it indicates that multiple organizations use the software as
infrastructure in roughly the same way, and thus the same person may
continue representing those technical priorities across different
organizations.
<br />
In other words, the difference between this and B2B Open Source is that less
organizational and process overhead is required to take part --- enough
less to make participating a qualitatively different proposition.  
For example, individual developers are generally more able and more
likely to make contributions to Kubernetes and OpenStack than they are
to Android, but they still tend to do so from organizational
rather than from individual motivation.  
These organizational motivations often relate to tactical needs, and may be enabled by
mid-level management decisions.  
By contrast, B2B open source tends to be driven by corporate business strategy, and is thus more likely to
figure into executive-level decisions.

* **Examples**  
  Kubernetes, Open Stack

* **Characteristics**  
  Multi-Vendor Infrastructure are large software projects that provide
  fundamental facilities to a wide array of other businesses.  
  This infrastructure is designed collaboratively by multiple vendors and
  then each deploys it in their own way.  
  Kubernetes and OpenStack are two prominent Multi-Vendor Infrastructure projects that most developers
  will have heard of.

* **Community standards**  
  Welcoming, but formal, and often difficult for individual contributors to enter.  
  Participation takes a high level of resources.  

* **Typical governance**  
  Formal tech committee, with membership by each of the major contributing companies.


<br />
### Rocket Ship To Mars

Rocket Ships To Mars tend not to play well with others.  
Every ounce of fuel goes to thrusters, which leads
little energy left over for the hard work of nurturing a contributor
community.
<br />
Rocket Ships To Mars typically do not invest a lot of effort
in encouraging broad contributorship, not so much because they
wouldn't welcome contributions as because it is hard to find
contributors who share the project's intensity and specificity of
vision, and who are sufficiently aligned with the founders to take the
time to make their contributions fit in.  
<br />
These projects are also usually trying to reach a convincing alpha release as quickly as
possible, so the tradeoff between roadmap speed and contributor
development looks different for them than for other types of projects.

* **Examples**  
  Meteor, Signal

* **Characteristics**  
  "Rocket Ship To Mars" projects are characterized by a small full-time core team that is
  wholly focused on a well-articulated and highly specific goal.  
  They are often led by charismatic founders and enjoy a funding runway sufficient for bootstrapping.
  Their open source strategy is often rooted in a commitment to transparency and
  providing insurance: they want to instill confidence among
  developers and users in order to promote adoption, and being open source
  is one ingredient in doing that.

* **Community standards**  
  Difficult to enter; focused on the core group.

* **Typical governance**  
  Maintainer-led by the founding group.


<br />
### Controlled Ecosystem

Controlled Ecosystem efforts find much of their value in that
ecosystem.  
The core provides base value, but the varied contributions
across a healthy plugin ecosystem allow the project to address a much
larger and diverse set of needs than any one project could tackle
alone.  
Over time, these projects might see more and more of the core
functionality structured as plugins as the product becomes more
customizable and pluggable.  
This increasingly lets the plugins
determine the end-user experience, and the core project can eventually
become infrastructure.

* **Examples**  
WordPress, Drupal

* **Characteristics**  
Real community involvement, and diversity of
community motivations, but with a shared understanding that the
founder (or some credible entity) will act as "benevolent dictator".
Requires experienced open source community management on part of
project leaders and occasional willingness to compromise.  Works best
when technical architecture directly supports out-of-core innovation
and customization, such as via a plugin system.

* **Community standards**  
Welcoming, often with structures
  designed to promote participation and introduce new contributors.

* **Typical governance**  
Benevolent dictatorship, with
  significant willingness to compromise to avoid forks.


<br />
### Wide Open

A Wide Open project values investing in contributors relatively
highly: the core development team is generally willing to pay a high
opportunity cost (in terms of code not written or bugs not fixed) in
order to encourage or onboard a promising new contributor.  
<br />
The project's collaboration tools and processes reflect this
prioritization.  
For example, the experienced developers in the
project will still take the time to hold design discussions in
forums-of-record --- even when many of the individuals concerned are
co-located and could make the decisions in person --- in order to
publicly reinforce the possibility of participation by any other
self-nominated interested party.

* **Examples**  
Rust (present day), Apache HTTPD

* **Characteristics**
Wide Open projects actively welcome contributions
from any source, and tend to get contributions at all levels: from
individual developers providing one-off bug fixes to sustained
organizational contributors developing major features and being
involved in long-range project planning.  

* **Community standards**  
Very welcoming to contributors, to
  the point of having formalized systems for onboarding newcomers.

* **Typical governance**  
Group-based and more or less democratic.  
In practice, most decisions are made by consensus, but
voting is available as a fallback when consensus cannot be reached.  
There is usually a formal committer group to approve contributions
and anoint new committers.


<br />
### Mass Market

Mass Market might at first seem more like a description of an end
product than of a project archetype, but in fact there are important
ways in which being Mass Market has implications for how the project
is run.  
With developer-oriented software, it usually pays to listen
to the most vocal customers.  
But Mass Market projects are oriented
mainly toward non-technical users, and thus should be guided more by
explicit user research.

* **Examples**  
Firefox, LibreOffice, MediaWiki (due to Wikipedia instance)

* **Characteristics**  
Mass Market projects are often similar to
"Wide Open" projects, but they necessarily have a protective layer
around their contribution intake process.  
This is because there are
simply too many users and too many opinions for everyone to be heard
by the development team, and because the average technical
sophistication of the users is low (especially in terms of their
familiarity with the conventions of open source participation).  
This is
especially true in a commercial context where product experience and
time-to-market are highly sensitive.

* **Community standards**  
Fully open, but relatively brusque
  for the vast majority of users.  
Because of the large number of
  users, these projects evolve toward a users-helping-users pattern,
  rather than the development team serving as user support.  
The team
  itself can often seem somewhat distant or unresponsive.

* **Typical governance**  
Technical committee and/or formal committer group.


<br />
### Specialty Library

These libraries are fairly commoditized, as the set of capabilities for
such a library is fairly fixed.  
Any ssl or mp4 library is going to do
roughly the same thing.  Competing libraries might differ at the API
level, but there is no arms race over features.  
Given the lack of
opportunity for competitive advantage in developing one's own library,
most of those capable of contributing have every reason to do it as
part of an open source project that shares pooled development costs
and benefits.
<br />
Widespread adoption of a specific open source library can power
standardization work and also increase adoption of formal and informal
standards.  
Coupled with careful approaches to patents, this can open
up access to participants who aren't attached to companies with deep
pockets and large patent portfolios.
<br />
Specialty libraries look like open source projects with high barriers
to entry.  Anybody can submit a patch, but landing it might require a
high degree of expert work before it is accepted into core.  
Coding
standards are high.  
There is
often less developer outreach and hand-holding of new developers as
participants are largely assumed to be experienced and capable.  
In a
heavily patented area or one subject to export controls, acceptance
policies might be more complex and even involve legal review.

* **Examples**  
libssl, libmp4

* **Characteristics**  
Specialty libraries provide base functionality to a
set of functions whose implementation requires specialized knowledge
at the leading edge of research.  
E.g., developers shouldn't roll
their own crypto or codecs; instead, everyone benefits from easy
access to a pooled set of code built by experts.

* **Community standards**  
High barriers to entry, largely
  because contributors are expected to be experts.

* **Typical governance**  
Formal committer group that can
  grant committing privileges to new contributors.


<br />
### Trusted Vendor

The guiding principle behind the Trusted Vendor archetype is that
nobody can build everything.  Sometimes people just want a complete
solution from a vendor.  
Switching costs for such products are often
high, so choosing a proprietary solution could lock one into a
technology and a vendor.  
That lock-in and the related dependence
gives vendors power over their customers and users, which allows
vendors to overprice and underdeliver.  
Customers and users too often
find themselves trapped in a relationship that doesn't meet their
needs and sometimes feels abusive.
<br />
Open source is the antidote to vendor lock-in.  
First, it enables
competition.  
If a vendor fails or pivots or suddenly raises prices, a
functioning open source ecosystem can provide a replacement without
high switching costs.  
Second, open source ecosystems tend toward open
standards.  
Integration, custom development, or even switching all
come easier, faster and cheaper if the underlying solution makes good
use of common standards.
<br />
This connection between open source and risk-free vendor dependence is
strong enough to shift customer preferences.  
More and more
procurement processes are aimed at open source solutions precisely
because they avoid lock-in during long-term vendor relations.

* **Examples**  
MongoDB, Hypothes.is, Coral

* **Characteristics**  
Overall project direction steered by a central
party --- usually the founding organization --- but with
encouragement of an active commercial ecosystem based on the project,
thus requiring occasional restraint or non-competitive behavior on the
part of the primary maintainer.

* **Community standards**  
Users and contributors have input to
  roadmap, though they may not contribute directly.

* **Typical governance**  
Maintainer-led by the vendor.


<br />
### Upstream Dependency

For smaller or more specialized libraries, this is a bit like Wide
Open, but the experience for maintainers and contributors is
qualitatively different.  
When a significant portion of the user base
is able to participate technically, a lot of attention and investment
may be needed from each serious participant just to take part in
discussions, even when the number of parties in a discussion is small.  
Relative to Wide Open, there are fewer one-off or lightweight
contributors and more ongoing, engaged contributors.

* **Examples**  
OpenSSL, WebKit, and just about every small
  JavaScript library on GitHub

* **Characteristics**  
Intended as a building block for other
software, so the user base is made up of developers, and adoption
means third-party integration of your software into their products.

* **Community standards**  
Welcoming, and specifically amenable
  to one-time contributions.

* **Typical governance**  
Informal, maintainer-led,
  committer groups.


<br />
### Bathwater

Typically, the published code is not the organization's prized
technology.  
In some cases it is a one-off publication and thus likely
to become quickly outdated.  
In other cases, there may be a public
repository, but that repository is not where development occurs ---
instead it is just a container for periodic snapshots of particular
release points.  
Sometimes there is not even a public repository, and
the only thing published is a release tarball or a sequence of them.

Users of
Bathwater projects don't expect anything from upstream, because there
is no upstream anymore, but they can can consider their involvement as
a greenfield opportunity.  
If they are willing to invest in the code,
they can choose any one of the other archetypes and start building
toward it.

* **Characteristics**  
This is code dumped over the wall.  
It is
purely about distribution, not about any particular mode of
production.  
Think of it as the ground state of open source projects:
someone publishes code under a free license but invests no followup
effort into building open source dynamics.

* **Community standards**  
Community is dormant to non-existent.

* **Typical governance**  
None.


<br />
### Evolution From One Archetype To Another

Projects can change from one archetype to another, and often do.  
It
may be a deliberate and guided transformation, or it may be a gradual
and incremental evolution that is not driven by any one participant.
<br />
In general, transformations from Rocket Ship To Mars to some other
archetype are among the most common, because they reflect a natural
progression: from a small, focused, and unified core team working on
something new, to a position where more people are involved and have
broadening influence in the project.  
If those new arrivals
are mostly developers whose motives are immediate rather than
strategic, that transformation might be toward Specialty Library.  
If
they are developers using the code as part of other programs, it might
be toward Trusted Vendor or Upstream Dependency.  
If they are plugin
developers and API consumers, it might be toward Controlled Ecosystem.
If they are organizations or organizationally-motivated developers,
B2B or Multi-Vendor Infrastructure might be likely places to end up.
If they are users, or a combination of users and developers, that
might point the way toward Mass Market or Wide Open.
<br />
Rocket Ship To Mars is not the only starting point for archetype
transitions.  
A project might move from B2B to Upstream Dependency as
it becomes commoditized, for example, perhaps through competition or
perhaps just through rapid adoption.  
We will not attempt here to map
out the full combinatoric range of possible archetype transitions.
Having such a map would not be very useful anyway.  
What is useful
instead is to be always alert to the possibility of transformation,
and to ask, at strategic points in a project's life cycle, whether a
purposeful transformation might be called for, and whether there might
be bottom-up pressure pushing the project toward a particular new
archetype.

<br />
*Source of project archetypes is Mozilla Foundation's report on [Open Source Archetypes](https://blog.mozilla.org/wp-content/uploads/2018/05/MZOTS_OS_Archetypes_report_ext_scr.pdf), which is very much recommended reading for further information.*


<br />
# 4: Select your quest
* **"Just curious"**  
  Being in the IRC or Discord channels will let you know some of the things going on.  
  Some of the larger design decisions are discussed or summarised in the issue list or in the Rolling Agenda.
  Many projects will have something like this; if nothing is listed, asking in the respective IRC channel will usually yield references.

* **"Feel like helping out"**  
  Look through the issue list and/or the code and see if something strikes your fancy.  
  Possibly mention what you're planning in a channel before you begin, in case someone has good suggestions.  

* **Take on a role**  
  Ask, either in a channel or directly to project manager if the project needs help with something. If you have your own ideas about what to help with, so much the better.  
  But with your intended Class and Roles in hand, plus an indication of how much time you are willing to dedicate, we stand a fair chance of finding something which will benefit both the project in terms of work done and you in terms of worthwhile experience gained.  
  At 4+ hours a week, you can have recurring one-on-one meetings for feedback and evaluation with the project manager - it helps with coordination as well.  
  If below that, or if you simply prefer not having the meetings, either choose helpful but non-critical roles, or attach yourself to existing project members.  

* **Take on a position**  
  At 4+ hours a week and upwards, usually you'll perform a number of roles for the project - e.g. testing, documenting and developing a library.  
  There's traditional ways of doing these things, but we're quite flexible in how roles are put together.  
  The number one thing we're looking for in regular contributors is transparency and consistency.  Those alone will move a project forward without major obstacles, while inconsistency or a lack of communication can easily put a spanner in the works.  
  Interestingly, paying jobs have similar priorities, and we'll happily recommend you for past efforts should you be seeking something along those lines.  


<br />
# 5: Evaluate
Use your one-on-one meetings, peer meetings and/or mentor/mentee meetings to get feedback on your activities.  
Each role will have certain goals attached to it which are important to fulfill for that role, and in addition to this there's qualities in any team member that are wished for in every organization - yet it's seldom made clear, anywhere, what they are.  
Contrast this with Newguard, where we try to be clear about expectations, and to give feedback when it is asked for.  
It will, of course, be up to yourself to do the work necessary to improve.  


<br />
# 6: New Horizons

**Take on a new challenge**  
With proven effort in Newguard, you may have come to the attention of project managers for one of ICEI's rescue projects.  
These tend to be highly technical in one sense or another, and you will have team members surrounding you who are highly capable within those fields.  
Furthermore, they are likely to share their knowledge with you so as to make you more capable of helping.  
