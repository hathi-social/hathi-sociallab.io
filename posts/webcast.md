<!--
.. title: Web Casts
.. slug: web-casts
.. date: 2018-01-31 23:00:00 UTC+01:00
.. tags: list, webcast
.. category: 
.. link: 
.. description: 
.. type: text
-->

Twice monthly, New Guard hosts web casts at crowdcast.io in order to share knowledge with our volunteers and all like-minded people.  
<br />

This takes the form of a presentation, with chat and a few tools to make it easy to answer questions from the audience.  
<br />

Videos are created from each cast, so if you missed a cast, the presentation is still available.  
<br />

**Link**: [https://www.crowdcast.io/icei](https://www.crowdcast.io/icei)  
**Days**: Every 2nd and 4th monday of each month  
**Time**: 8PM EST  

<br />

------------------------------------------------------------------------------------------------------------------

<br />

**Upcoming web casts:**  
For up to date information, refer to the crowdcast link, above.  
Previous and upcoming casts are presented there as the entry page.  

<br />

------------------------------------------------------------------------------------------------------------------

<br />

**Previous web casts:**

<br />

| Date             | &nbsp;&nbsp;&nbsp; | Category              | &nbsp;&nbsp;&nbsp; | Web Cast                                                                               |
|:----------------:| ------------------ | --------------------- | ------------------ | -------------------------------------------------------------------------------------- |
| Apr 10, 2018     |                    | New Guard ProDev      |                    | [Project Management Will Save Your Life](https://www.crowdcast.io/e/technical-project) |
| Feb 26, 2018     |                    | New Guard Tech        |                    | [How to Read an RFC](https://www.crowdcast.io/e/-how-to-read-an-rfc)                   |
| Dec 12, 2017     |                    | New Guard ProDev      |                    | [Working with Recruiters](https://www.crowdcast.io/e/new-guard-professional-2)         |
| Nov 14, 2017     |                    | New Guard ProDev      |                    | [ProDev Episode 2](https://www.crowdcast.io/e/new-guard-professional)                  |
| Oct 24, 2017     |                    | New Guard Tech        |                    | [Linux Command Line for Programmers](https://www.crowdcast.io/e/new-guard-cli)         |
| Oct 10, 2017     |                    | New Guard ProDev      |                    | [Productivity For Hackers](https://www.crowdcast.io/e/new-guard-prodev)                |
