<!--
.. title: Projects
.. slug: newguard-projects
.. date: 2017-10-15 15:20:23 UTC-04:00
.. tags: openings, positions, projects
.. category: 
.. link: 
.. description: 
.. type: text
-->


## FabriCode
Some of Icei's other projects need a high level of security and access control, since their functioning or lack of same has direct implications for the functioning of the Internet.  
None of the current git repository managers implement access control to a suffiently high degree, making collaboration more difficult and time-consuming than it needs to be.  
<br />
The purpose of FabriCode is to remedy this, and implement a git repository with fine-grained access control.  

### Openings

* 

-----------------------------------------------------------------------

<br />

## Hathi
At the moment, all widely used collaboration platforms are commercial.  
This leads to unnecessary fragmentation of expertise, and voluntary groups being dependent on communication mediums which constitute, in essence, single points of failure.  
For Icei, collaboration and free exchange of expertise is a must; furthermore the medium over which it happens must be trusted and stable.  
<br />
The purpose of Hathi is to implement a decentralized, stable and secure communications and collaboration network, primarily focused on the needs of developers but with a view towards general collaboration and communication too.  

### Technology & Standards
* Programming language for server: [Go](https://golang.org/doc)
* Programming language for client: [Python3](https://docs.python.org/3)
* Protocol: [ActivityPub](https://www.w3.org/TR/activitypub)

### Openings
* **Go developers**  
  First prototype server is being written in Go

* **Frontend developers**  
  We will soon be ready to have our server tested


### More information

* **Planning wiki**  
  [https://gitlab.com/hathi-social/planning/wikis/home](https://gitlab.com/hathi-social/planning/wikis/home)

* **Repositories**  
  [Hathi server](https://gitlab.com/hathi-social/hathi-server)  
  [Hathi test client](https://gitlab.com/hathi-social/hathi-client)

* **Discussions**  
  Discussions are for the most part happening or getting summaries on the mailing list;
  Planning documents may be added to the [planning repository ](https://gitlab.com/hathi-social/planning) in the future.


### Contact

* **IRC**  
  \#hathi on irc.freenode.net

* **Contact person**  
  uw (at) icei.org

* **Mailing list**  
  hathi@lists.icei.org

<!--
- Information about which projects are looking for developers or other help
- What those projects are about
- What such a role might consist of
- Why one might like working on those projects
-->
