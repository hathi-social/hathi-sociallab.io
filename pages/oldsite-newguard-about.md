<!--
.. title: New Guard
.. slug: newguard-about
.. date: 2017-10-15 15:20:33 UTC-04:00
.. tags: about, newguard
.. category: 
.. link: 
.. description: 
.. type: text
-->

## Today's rookies are tomorrow's veterans.

New Guard is a mentoring group for early- and mid-career technologists learning to work on infrastructure software.

New Guard provides a venue for early- and mid-career technologists and tech-cohorts of all stripes to cross-mentor and build skills for working on infrastructure software. We also do matchmaking for Newguardians with old-school infrastructure software maintainers to give the New Guard an opportunity to work under, and learn from, those who came before them.

<br />

## Our Mission

Today, universities and code camps are churning out programmers at a prodigious rate. They teach young people to build web apps, mobile apps, games and new "big data" processing systems. They teach some algorithms and one or two high-level programming languages. Unfortunately, they aren't teaching people to take care of the code we most rely on, because it's the most ubiquitous: systems software.

Working on the software that makes the internet, and general-purpose computing, possible is a different skill set. It requires specific languages: usually C, and occasionally C++, Rust, or Go. More importantly, it requires a level of rigor in architecture and development processes unlike most programming disciplines. No one teaches this.

<br />

## Newguard's founder

New Guard's founder, ICEI Hacker-in-Chief Susan Sons, learned these skills growing up amid a loose-knit group of Open Source communities. However, as she learned some years into her career, too few people receive this sort of mentorship. As a result, the infrastructure software world is having a manpower crisis.

Susan's story, as told at a NewGuard meetup in 2015:

Something funny happened a few years ago. Google came out with a feature by which a user can have his or her account turned over to a third party after a certain period of inactivity, to essentially will it to someone . A few infrastructure devs set their accounts to go to me.


> "Why?" I asked. "Why me instead of your co-developer, children, or spouse?"

> "You", they answered, "Are the only one of us under 30".

> I thought about it a while, and I started asking around that community,  
> "What happens to your software when you die?"

> Paul Vixie had a good answer. Eric Raymond had a good answer.  
> Everyone else answered along the lines of "Susan, you'll figure it out".

> I looked around for help: there had to be other people my age willing  
> and ready to take the hand-off. There weren't.

> One of my coworkers, a bright Linux hacker named Mark Krenz, started  
> calling me "the godmother of the internet". That's when it hit me:  
> Someday these guys are really going to start dying off, and I cannot  
> maintain so much computing infrastructure by myself.

> *Cue moment of abject terror.*

> I find myself standing in a strange and wonderful place, a sort of bridge  
> between generations.  
> I'm now 32 years old, but the programmers I work with are all closer  
> to my father's age than mine. I'm part of this younger generation,  
> but I was brought up by the old, and I think like them.

> I started New Guard to give my age peers what I got by stumbling onto  
> USENET and IRC when I was 12, and being brought up by the fathers of  
> the internet as I know it.

> We don't just need a few good infrastructure developers:  
> we need a strong community of practice that not just includes skilled  
> developers, toolchain maintainers, test engineers, and project managers,  
> but creates them.  
> We need strong lines of succession within our community, and we need  
> to learn enough of our history to stand on the shoulders of giants  
> rather than re-inventing the wheel in each generation, never having  
> the resources to move forward.

Susan spoke about information security, the manpower crisis in open source infrastructure software, and more in an interview with Mac Slocum at O'Reilly's Security Conference 2016.

<br />

## Newguard structure

New Guard is a development and maintenance group as much as it is a mentoring group.
Initiatives are set into motion with several goals in mind:

* To keep the internet operational
* To promote the values of the open source movement
* To mentor future OSS volunteers

<br />

### Qualifications

* You do not need to be an infrastructure software specialist in order to join New Guard. We need future Project Managers, Documentation Specialists, and Community Managers as much as we need future technical staff.
* If your aim is to work on the technical sides of these projects, then you should come in knowing how to maintain your own workstation and development tools when working from Linux, UNIX, or Mac OSX, how to bisect a problem, how to read and learn from good documentation, how to ask questions the smart way, and how to communicate clearly and effectively with others.
* If a sysadmin, you should have strong Linux/UNIX CLI skills.
* If a programmer, you should have worked in one or two programming languages on production code, i.e. code that is deployed and relied on by someone other than yourself.

<br />

### Contact
The best way to connect with New Guard is to join our IRC channel, #newguard on irc.freenode.net.  
For other options, see Contact Information on the Starter Guide page.
