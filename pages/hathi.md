<!--
.. title: Hathi
.. slug: index
.. date: 2018-04-14 09:00:00 UTC+02:00
.. tags: openings, positions, projects
.. category: 
.. link: 
.. description: 
.. type: text
-->


At the moment, all widely used collaboration platforms are commercial; this is a problematic state of affairs.  
It leads to unnecessary fragmentation of expertise.  
It leads to voluntary groups being dependent on communication mediums which constitute, in essence, single points of failure.  
<br />
The purpose of Hathi is to implement a decentralized, stable and secure communications and collaboration network, primarily focused on the needs of developers but with a view towards general collaboration and communication as well.  

### Technology & Standards
* Programming language for server and router: [Go](https://golang.org/doc)
* Programming language for identity manager: C++
* Programming language for client: [Python3](https://docs.python.org/3)
* Base architecture and protocol: [ActivityPub](https://www.w3.org/TR/activitypub)

### Openings
* **Go developers**  
  The server is being written in Go

* **Python developers**  
  The client and test client is being written in Python3

* **Frontend developers**  
  We are going to want to make a reference frontend in order to show off a web client  
  Ie, the functional equivalent of a social site

### State of things (March, 2019)
* Overall architecture has been designed
* Server network has been designed and is nearly fully implemented
* Identity management has been implemented for prototyping and testing purposes
* Router is being implemented
* Command-line client for testing and prototyping is in the works
* Local server messaging is working
* Server-to-server propagation is being implemented
* Protocol and API between servers and clients has been implemented
* Automated testing is being implemented along with each new API functionality  


### More information

* **Planning**  
  [Planning wiki - Problem Statement, Mission Statement, Milestones](https://gitlab.com/hathi-social/planning/wikis/home)  
  [Rolling Agenda - Summaries of Meetings](https://docs.google.com/document/d/1qhe5NdzJW4b9yYHvbezl-4r7NxTo96gt8FWUiliSBnI/edit#heading=h.poraq1os5l33)  
  [Gitlab Issue List](https://gitlab.com/groups/hathi-social/-/issues)  

* **Testing**  
  [Coming: Install instructions for server]  
  [Coming: Executing test module]  

* **Discussions**  
  Daily discussions are being done via Discord: [Hathi on Discord](https://discord.gg/wyaZNzv)  
  We are also present at #hathi on IRC/Freenode.  
  We have weekly team meetings, which are summarized in our [Rolling Agenda](https://docs.google.com/document/d/1qhe5NdzJW4b9yYHvbezl-4r7NxTo96gt8FWUiliSBnI/edit#heading=h.poraq1os5l33)  


### Contact

* **IRC**  
  \#hathi on irc.freenode.net

* **Contact person**  
  hadaqada (at) gmail.com - Project Manager
